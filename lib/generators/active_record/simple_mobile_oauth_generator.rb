require 'rails/generators/active_record'

module ActiveRecord
  module Generators
    class SimpleMobileOauthGenerator < ActiveRecord::Generators::Base
      source_root File.expand_path("../templates", __FILE__)

      def append_to_token_authenticatable_model
        inject_into_class "app/models/#{name}.rb", name.camelize.constantize, <<-END
  include SimpleMobileOauth::Identifiable
        END
      end
    end
  end
end
